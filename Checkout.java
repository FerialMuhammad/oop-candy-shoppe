import java.util.ArrayList;

public class Checkout {

    protected ArrayList<DessertItem> items;


    public Checkout(){

        this.items=new ArrayList<DessertItem>();

    }
    public void clear(){
        this.items.clear();

    }

    public void enterItem(DessertItem item){

        this.items.add(item);

    }
    public int numberOfItems(){
        return(this.items.size());

    }
    public java.lang.String toString(){
        String s=DessertShoppe.STORE_NAME + "\n";
        s=s+ "---------------\n";

        for(int i=0; i<items.size();i++){
            s=s+(items.get(i).name)+"\t";
            s=s+DessertShoppe.cents2dollarsAndCents((items.get(i).getCost())) + "\n";
            if (items.get(i) instanceof Candy){
                s=s+(((Candy) items.get(i)).getWeight())+" lbs. @ ";
                s=s+DessertShoppe.cents2dollarsAndCents(((Candy) items.get(i)).priceCa)+"/lb. \n";
            }
            if (items.get(i) instanceof Cookie){
                s=s+(((Cookie) items.get(i)).getNumber())+" @ ";
                s=s+ (DessertShoppe.cents2dollarsAndCents(((Cookie) items.get(i)).priceC))+"/dz. \n";
            }
        }
        s+= "Tax \t" + DessertShoppe.cents2dollarsAndCents(this.totalTax());
        s+= "\nTotal Cost \t"+DessertShoppe.cents2dollarsAndCents(this.totalCost());
        return s;

    }
    public int totalCost(){
        int total=0;
        for(int i=0; i<items.size();i++){
            total+=(items.get(i)).getCost();
        }
        return total;

    }
    public int totalTax(){
        double Tax=this.totalCost()* (DessertShoppe.TAX_RATE/100);
        return (int) Tax;

    }



}
