class Cookie extends DessertItem{

    protected  int numberC;
    protected int priceC;

    Cookie(String nameC,int numberC, int priceC){
        this.name=nameC;
        this.numberC=numberC;
        this.priceC=priceC;
    }


    public int getCost(){
        double cost= (this.numberC/12.0)*this.priceC;
        return (int) cost;

    }
    public int getNumber(){
        return this.numberC;
    }
}
