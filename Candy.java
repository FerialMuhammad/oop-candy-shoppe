class Candy extends DessertItem {

    protected double weightCa;
    protected int priceCa;

    Candy(String nameCa,double weightCa, int priceCa){

        this.name=nameCa;
        this.weightCa=weightCa;
        this.priceCa=priceCa;
    }

    public int getCost(){
        double cost=this.weightCa*this.priceCa;
        return (int) cost;

    }
    public double getWeight(){
        return this.weightCa;
    }
}
