class IceCream extends DessertItem {

    protected int cost;

    IceCream(String nameI,int cost){
        this.name=nameI;
        this.cost=cost;
    }

    public int getCost(){

        return cost;
    }
}
