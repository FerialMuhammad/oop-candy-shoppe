public class Sundae extends IceCream {

    protected String nameT;
    protected int costT;

    Sundae(String nameI,int costI,String nameT,int costT){
        super(nameI,costI);
        this.nameT=nameT;
        this.costT=costT;
    }
    public int getCost(){
        int cost = this.cost+this.costT;
        return cost;
    }


}
